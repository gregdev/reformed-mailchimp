<?php

/**
 * Plugin Name: Reformed MailChimp
 * Version:     1.0
 * Author:      Greg Smith
 * Author URI:  https://gregdev.com.au
 * Description: A "subscribe to MailChimp" action.
 */

class reformed_mailchimp {

    const ACTION_NAME = 'MailChimp Subscribe';

    public static function init() {
        add_action('reformed/actions/register', function() {
            reformed_actions::register(self::ACTION_NAME, 'reformed_mailchimp::subscribe', [
                [
                    'type'      => 'text',
                    'interface' => 'textbox',
                    'name'      => 'MailChimp API Key',
                ],
                [
                    'type'      => 'text',
                    'interface' => 'textbox',
                    'name'      => 'MailChimp Audience ID',
                ],
                [
                    'type'      => 'text',
                    'interface' => 'select',
                    'source'    => 'field_blocks',
                    'name'      => 'Email Field',
                ],
                [
                    'type'      => 'text',
                    'interface' => 'select',
                    'source'    => 'field_blocks',
                    'name'      => 'First Name Field',
                ],
                [
                    'type'      => 'text',
                    'interface' => 'textbox',
                    'name'      => 'Tag',
                ],
            ]);
        });
    }

    public static function subscribe(reformed_form $form, $data) {
        $options        = reformed_actions::get_options($form, self::ACTION_NAME);
        $email_address  = $data['reformed_field'][$options['email-field']];
        $first_name     = $data['reformed_field'][$options['first-name-field']];
        $tag            = $data['reformed_field'][$options['tag']];

        self::mailchimp_subscribe($options['mailchimp-api-key'], $options['mailchimp-audience-id'], $email_address, $first_name);

        if ($tag) {
            self::mailchimp_tag($options['mailchimp-api-key'], $options['mailchimp-audience-id'], $email_address, $tag);
        }
    }

    protected static function mailchimp_subscribe($api_key, $audience_id, $email_address, $first_name = '') {
        $email_address  = strtolower($email_address);
        $endpoint       = "lists/$audience_id/members/" . md5($email_address);

        $data = [
            'email_address' => $email_address   ,
            'status'        => 'subscribed'     ,
            'merge_fields'  => (object) []      ,
        ];

        if ($first_name) {
            $data['merge_fields']->FNAME = $first_name;
        }

        return self::mailchimp_request($api_key, $endpoint, $data);
    }

    protected static function mailchimp_tag($api_key, $audience_id, $email_address, $tag) {
        $email_address  = strtolower($email_address);
        $endpoint       = "lists/$audience_id/members/" . md5($email_address) . "/tags";

        $data = [
            'name'      => $tag,
            'status'    => 'active',
        ];

        return self::mailchimp_request($api_key, $endpoint, $data);
    }

    protected static function mailchimp_request($api_key, $endpoint, $data) {
        $ch     = curl_init();
        $url    = 'https://' . substr($api_key, strpos($api_key, '-') + 1) . ".api.mailchimp.com/3.0/$endpoint";

        curl_setopt_array($ch, [
            CURLOPT_URL             => $url,
            CURLOPT_USERAGENT       => 'WordPress/Reformed',
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_CUSTOMREQUEST   => 'PUT',
            CURLOPT_POSTFIELDS      => json_encode($data),
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_HTTPHEADER      => [
                'Content-Type: application/json',
                'Authorization: Basic ' . base64_encode('user:' . $api_key),
            ],
        ]);

        $result = curl_exec($ch);

        return $result;
    }

}

reformed_mailchimp::init();